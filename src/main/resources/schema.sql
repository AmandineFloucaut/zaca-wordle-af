CREATE TABLE if NOT EXISTS users(
tid char(36) NOT NULL PRIMARY KEY,
email text NOT NULL,
username text NOT NULL
);

CREATE TABLE if NOT EXISTS game(
tid char(36) PRIMARY KEY,
word text NOT NULL,
max_attempts int NOT NULL,
user_id char(36) REFERENCES users(tid)
);

CREATE TABLE if NOT EXISTS  round_result(
word text NOT NULL,
round_order int NOT NULL,
game_tid char(36) REFERENCES game(tid),
PRIMARY KEY(game_tid, round_order)
);