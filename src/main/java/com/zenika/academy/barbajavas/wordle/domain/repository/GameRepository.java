package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public interface GameRepository extends CrudRepository<Game, String> {

    List<Game> findByUserId(String usersId);

    //Boolean userOwnsGame(String userTid, Game g);
}
