package com.zenika.academy.barbajavas.wordle.domain.model.users;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="users")
@Access(AccessType.FIELD)
public class User {
    @Id
    private String tid;
    private String email;
    private String username;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "tid")
    private List<Game> games;

    protected User(){}
    public User(String tid, String email, String username) {
        this.tid = tid;
        this.username = username;
        this.email = email;
    }

    public String getTid() {
        return tid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        if(!StringUtils.hasText(username)) {
            throw new IllegalArgumentException("Username can't be null");
        }
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(!StringUtils.hasText(email)) {
            throw new IllegalArgumentException("Email can't be null");
        }
        this.email = email;
    }
}
