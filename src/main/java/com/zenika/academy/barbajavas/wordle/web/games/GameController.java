package com.zenika.academy.barbajavas.wordle.web.games;

import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.domain.model.game.GameNotForThisUserException;
import com.zenika.academy.barbajavas.wordle.domain.model.users.UserNotFoundException;
import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.GameFinishedException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/games")
public class GameController {
    
    private final GameManager gameManager;

    @Autowired
    public GameController(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @PostMapping
    ResponseEntity<Game> createGame(@RequestParam Integer wordLength, @RequestParam Integer maxAttempts, @RequestParam(required = false) String userTid) throws UserNotFoundException {
        if(wordLength < 3 || maxAttempts < 2) {
            return ResponseEntity.badRequest().build();
        }
        
        return ResponseEntity.ok(gameManager.startNewGame(wordLength, maxAttempts, userTid));
    }
    
    @PostMapping("/{gameTid}")
    ResponseEntity<Game> guess(@PathVariable String gameTid, @RequestBody GuessRequestDto guessRequestDto) throws UserNotFoundException, GameNotForThisUserException {
        try {
            return ResponseEntity.ok(gameManager.attempt(gameTid, guessRequestDto.guess(), guessRequestDto.userTid()));
        } catch (IllegalWordException | BadLengthException | GameFinishedException e) {
            return ResponseEntity.badRequest().build();
        }
        catch (IllegalArgumentException e) {
            return ResponseEntity.notFound().build();
        }
    }
    
    @GetMapping("/{gameTid}")
    ResponseEntity<Game> getGame(@PathVariable String gameTid) {
        return gameManager.getGame(gameTid)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    } 
    
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleUserNotFound(UserNotFoundException e) {
        return "User not found";
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public String handleForbidden(GameNotForThisUserException e) {
        return "Forbidden : " + e.getMessage();
    }
}
