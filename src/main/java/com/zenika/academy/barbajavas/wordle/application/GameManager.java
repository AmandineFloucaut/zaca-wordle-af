package com.zenika.academy.barbajavas.wordle.application;

import com.zenika.academy.barbajavas.wordle.domain.model.game.GameNotForThisUserException;
import com.zenika.academy.barbajavas.wordle.domain.model.users.UserNotFoundException;
import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.game.GameState;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.DictionaryService;
import com.zenika.academy.barbajavas.wordle.domain.service.GameFinishedException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class GameManager {

    private final DictionaryService dictionaryService;
    private final GameRepository gameRepository;
    private final UserService userService;

    @Autowired
    public GameManager(DictionaryService dictionaryService, GameRepository gameRepository, UserService userService) {
        this.dictionaryService = dictionaryService;
        this.gameRepository = gameRepository;
        this.userService = userService;
    }

    public Game startNewGame(int wordLength, int nbAttempts, String userTid) throws UserNotFoundException {
        if (userTid != null && userService.findByTid(userTid).isEmpty()) {
            throw new UserNotFoundException();
        }

        Game game = new Game(UUID.randomUUID().toString(), dictionaryService.getRandomWord(wordLength), nbAttempts, userTid);
        gameRepository.save(game);
        return game;
    }

    public Game attempt(String gameTid, String word, String userTid) throws IllegalWordException, BadLengthException, GameFinishedException, GameNotForThisUserException, UserNotFoundException {
        Game game = gameRepository.findById(gameTid)
                .orElseThrow(() -> new IllegalArgumentException("This game does not exist"));

        if (userTid != null && userService.findByTid(userTid).isEmpty()) {
            throw new UserNotFoundException();
        }

        if (!dictionaryService.wordExists(word)) {
            throw new IllegalWordException();
        }
        if (word.length() != game.getWordLength()) {
            throw new BadLengthException();
        }
        if (game.getGameState() != GameState.IN_PROGRESS) {
            throw new GameFinishedException();
        }
        
        game.guess(word, userTid);

        gameRepository.save(game);

        return game;
    }

    public Optional<Game> getGame(String gameTid) {
        return gameRepository.findById(gameTid);
    }
}
