package com.zenika.academy.barbajavas.wordle.web.users;

public record ChangeUsernameRequestDto(String newUsername) {
}
