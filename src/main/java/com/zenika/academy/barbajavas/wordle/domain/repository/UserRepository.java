package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
    /*public void save(User u);

    public Optional<User> findByEmail(String email);

    public Optional<User> findByTid(String userTid);

    public void delete(String userTid);*/
    Optional<User> findByEmail(String email);
    void deleteById(String userTid);
}
