package com.zenika.academy.barbajavas;

import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

@SpringBootApplication
public class WordleApplication {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Bean
    public I18n i18n(@Value("${wordle.language}") String language) throws Exception {
        return I18nFactory.getI18n(language);
    }
    
    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }
    
    public static void main(String[] args) throws Exception {
        SpringApplication.run(WordleApplication.class);
    }

    @PostConstruct
    void testJdbc(){
        User u = jdbcTemplate.queryForObject("SELECT * FROM users WHERE tid=?", new UserRowMapper(), "4930e334-ba70-11ec-8422-0242ac120002");
        //System.out.println(jdbcTemplate.queryForObject("SELECT 'Hello World'", String.class));
        System.out.println(u);
    }

    public class UserRowMapper implements RowMapper<User>{
        @Override
        // mapRow - https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/jdbc/core/RowMapper.html
        public User mapRow(ResultSet result, int rowNum) throws SQLException {
            final String userTid = result.getString("tid");
            final String email = result.getString("email");
            final String username = result.getString("username");
            return new User(userTid, email, username);
        }
    }
}
